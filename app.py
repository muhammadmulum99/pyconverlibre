from flask import Flask, request, jsonify
import os
import subprocess
import base64
from werkzeug.utils import secure_filename

app = Flask(__name__)
UPLOAD_FOLDER = 'uploads'
OUTPUT_FOLDER = 'outputs'
os.makedirs(UPLOAD_FOLDER, exist_ok=True)
os.makedirs(OUTPUT_FOLDER, exist_ok=True)

@app.route('/convert', methods=['POST'])
def convert():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part in the request'}), 400

    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No file selected for uploading'}), 400

    if file:
        # Simpan file sementara di server
        filename = secure_filename(file.filename)
        input_path = os.path.join(UPLOAD_FOLDER, filename)
        file.save(input_path)

        # Tentukan output path untuk PDF
        output_filename = os.path.splitext(filename)[0] + '.pdf'
        output_path = os.path.join(OUTPUT_FOLDER, output_filename)

        try:
            # Gunakan subprocess untuk menjalankan perintah LibreOffice
            subprocess.run(['C:\\Program Files\\LibreOffice\\program\\soffice.exe', '--headless', '--convert-to', 'pdf', '--outdir', OUTPUT_FOLDER, input_path], check=True)

            # Baca file PDF yang telah dikonversi dan ubah menjadi base64
            with open(output_path, "rb") as pdf_file:
                pdf_bytes = pdf_file.read()
                pdf_base64 = base64.b64encode(pdf_bytes).decode('utf-8')

            # Kirim response sebagai JSON dengan data base64
            return jsonify({'file': pdf_base64, 'filename': output_filename})
        except subprocess.CalledProcessError as e:
            print(f"Conversion error: {e}")
            return jsonify({'error': 'Conversion failed'}), 500
        finally:
            # Hapus file sementara
            if os.path.exists(input_path):
                os.remove(input_path)
            if os.path.exists(output_path):
                os.remove(output_path)

if __name__ == '__main__':
    app.run(debug=True)
