#NamaKotaFFSatelit, #tanggalcetakordernotaris
No. #NomorAplikasiPembiayaanpadaNOS
Lampiran : 1 berkas

Kepada,								
Kantor Notaris/PPAT #fieldnamanotaris/PPAT	
Di #fieldalamatnotaris/PPAT

U.p : Yth. #namanotaris/PPAT


Perihal:  Pembuatan Akta-akta Notaril dan/atau PPAT

Assalaamu’alaikum  Wr  Wbr 

Semoga Saudara beserta seluruh staf senantiasa dalam keadaan sehat wal’afiat dan mendapatkan taufik serta hidayah dari Allah SWT.

Menunjuk perihal tersebut diatas dengan ini kami mengharap bantuan Saudara untuk melaksanakan akta-akta notaris dan/atau PPAT untuk kepentingan PT Bank Syariah Indonesia,Tbk  dengan data sebagai berikut:

Nasabah	
Nama	: #fieldnamasesuaiidentitas
		Alamat nasabah	: #fieldalamatsesuaiidentitas

Pembiayaan	
Judul Akad	: 	Akad Pembiayaan #fieldjenisakad
Jenis Pembiayaan	:	#fieldjenisakad
Limit Pembiayaan	:	#fieldlimityangdisetujui
Tujuan penggunaan	:	#fieldtujuanpembiayaan, #objekpembiayaan
Jangka waktu	:	#fieldtenoryangdisetujui

Objek / Agunan pembiayaan	
Objek Pembiayaan	: #fieldobjekagunan
Status Objek	: #fieldstatusobjek
Bukti Kepemilikan	: #fieldjenissertifikat
Nomor Seritifikat/NIB	: #fieldnomorsertifikat/nib
Nama Pemegang sertifikat	: #fieldnamapemeganghak
Alamat	: #fieldalamatagunan
Peringkat Hak Tanggungan	: 1 (satu)
Nilai Hak tanggungan	: #fieldlimityangdisetujui

Berdasarkan data-data tersebut di atas, syarat dan ketentuan yang tercantum dalam Surat Penawaran Pemberian Pembiayaan (SP3) Nomor #fieldNo.sp3 Tanggal #fieldtanggalsp3 perihal Surat Penawaran Pemberian Pembiayaan atas nama #fieldnamasesuaiidentitas yang merupakan satu kesatuan yang tidak dapat dipisahkan dari Akad Pembiayaan, harap Saudara membuat dan melaksanakan akta-akta notaris dan/atau PPAT sebagai berikut* :

[sesuai pilihan pekerjaan notaris]
[sesuai pilihan pekerjaan notaris]
[sesuai pilihan pekerjaan notaris]
[sesuai pilihan pekerjaan notaris]
[sesuai pilihan pekerjaan notaris]
Paling banyak 10 pekerjaan, akan keluar sebagai list sesuai dengan pilihan pekerjaan notaris pada stage order notaris

Sebagai dasar pembebanan untuk pengikatan agunan (pekerjaan 3) adalah berdasarkan Akad pembiayaan yang Saudara buat / Akad Pembiayaan yang dibuat dan ditandatangan secara bawah tangan.
	
Pihak yang akan berkomparisi mewakili Bank untuk penandatangan akad dan pengikatan :
Nama 	: #fieldnamapejabatpenandatangananakad
Surat Kuasa 	: Nomor #fieldnomorsuratkuasapejabatpenandatangan tanggal #tanggalsuratkuasa
SKPP 	:Nomor #fieldnomorSKPPPejabatPenandatanganakad tanggal #tanggalsuratkeputusan


Pelaksanaan Akad dan Pengikatan :
Tempat Akad	:	#fieldlokasiakad
Tanggal 	:	#fieldtanggalpelaksanaanakad
Pukul	: #fieldwaktupelaksaanakad

Contact Person: 
Bank		  
Nama 	: #fieldnamamarketing
Jabatan	: Marketing
No. Telp	: #fieldnomortelpmarketing
Nasabah
Nama	: #fieldnamasesuaiidentitas
No Telp	: #fieldnomorhandphone
  
Dokumen-dokumen yang Saudara perlukan untuk proses tersebut di atas akan diberikan kepada Saudara sebelum pelaksanaan penandatanganan akad dan pengikatan agunan, dibuktikan dengan Berita Acara Serah Terima, jika dokumen dimaksud belum diterima mohon agar tidak melakukan penandatanganan akad dan/atau pengikatan agunan.

Sebelum penandatanganan akad dan pengikatan agunan dimaksud,  Anda wajib melakukan pengecekan/penelitian legalitas atas dokumen-dokumen sebagai berikut:

Keaslian dokumen yaitu :
Identitas (KTP) para penghadap yang menandatangani akta-akta
Dokumen kepemilikan  jaminan dan dokumen pendukung lainnya

Telah dilakukan penelitian/pengecekan ke Kantor Pertanahan setempat bahwa sertifikat tanah tidak bermasalah dan tidak dalam sengketa serta dapat dilaksanakan pengikatan secara sempurna.

Memastikan seluruh kewajiban kepada Negara (Pajak dan lainnya) telah dilaksanakan oleh para pihak sebelum pelaksanaan penandatanganan akad.
	
Sehubungan dengan hal tersebut, kami mengharapkan agar Saudara melaksanakan hal-hal sebagai berikut:

Menegaskan di Akta yang Saudara buat bahwa Penyelesaian Perselisihan di Pengadilan Agama dengan memilih tempat kedudukan yang umum dan tetap pada Kantor Kepaniteraan Pengadilan Agama yang wilayahnya meliputi keberadaan Cabang Bank yang memberikan pembiayaan ini.

Menyerahkan draft akta minimal H-1 sebelum penandatanganan akta dilaksanakan, agar dapat dilakukan review oleh Bank terhadap draft akta tersebut. 

Pada saat pelaksanaan akad, Notaris wajib hadir ditempat akad dan tidak dapat diwakilkan kepada Staf Kantor Notaris baik dengan sepengetahuan Bank maupun tidak.

Menyerahkan surat keterangan (covernote) tentang pengurusan Akta dan Pengikatan Agunan disertai dengan kesanggupan batas waktu penyelesaian yang ditujukan kepada:

Financing Factory 	: Jakarta	
Alamat	: The Tower, Jalan Gatot Subroto #dummydataAlamat, jika terdapat perubahan akan dilampiran pada template yang baru
Nomor telp	: 08123456788910 #dummydataTelp, jika terdapat perubahan akan dilampiran pada template yang baru
Alamat Email	: bankbsi.co.id #dummydataemail, jika terdapat perubahan akan dilampiran pada template yang baru

Standard covernote Notaris maupun isi covernote sesuai dengan lampiran

Apabila Saudara telah menerbitkan covernote, berarti bahwa semua dokumen/data yang terkait dengan proses tersebut telah Saudara terima dan clear, sehingga apabila di kemudian hari terjadi masalah menjadi tanggung jawab Saudara. 

Memberikan pelayanan dan jasa yang terbaik kepada PT Bank Syariah Indonesia, Tbk sesuai ketentuan perundang-undangan yang berlaku, dengan menjunjung tinggi profesionalisme dan etika profesi.

Menyerahkan semua salinan akta (berikut doorslag / copy salinan) dan dokumen agunan untuk diserahkan kepada Regional Financing Factory Jakarta sesuai komitmen Saudara sebagaimana yang tercantum di dalam Surat Keterangan/Covernote.

Menyelesaikan order pekerjaan dalam jangka waktu sebagai berikut:


SLA terhitung sejak penandatanganan akad pembiayaan

Memberikan laporan rutin setiap bulan pada minggu pertama, selama penugasan ini belum selesai dengan mengemukakan permasalahan yang dihadapi (apabila ada) yang diserahkan kepada  Financing Factory [Jakarta/Aceh/Makasar]  dengan tembusan kepada PT Bank Syariah Indonesia, Tbk #Fieldnamacabang

Bank akan meninjau pemberian order pekerjaan/kerja sama rekanan dan menerapkan sanksi yang telah disetujui dan ditandatangani oleh Saudara dalam Surat Pernyataan (Undertaking Guarantee) apabila Saudara tidak  melaksanakan hal-hal yang kami minta untuk dilaksanakan seperti tersebut di atas.

Dalam pelaksanaan Good Corporate Governance (GCG), kami mengharap agar Saudara tidak memberikan dan atau menjanjikan sesuatu dalam bentuk apapun juga kepada pejabat dan atau petugas PT Bank Syariah Indonesia, Tbk berkenaan  dengan pelaksanaan pekerjaan yang ditugaskan kepada Saudara.

Demikian kami sampaikan, atas perhatian dan kerjasama yang baik kami mengucapkan terimakasih.


Wassalaamu’alaikum Wr. Wbr.


Note : surat order ini dibuat dan dicetak secara elektronik, sehingga tidak diperlukan tandatangan pejabat bank.

Menerima & menyetujui
Notaris/PPAT 






#fieldnamanotaris


Tembusan:    	PT Bank Syariah Indonesia, Tbk #Fieldnamacabang

